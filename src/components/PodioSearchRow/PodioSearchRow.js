import React, { Component } from 'react';
import './PodioSearchRow.scss';

class PodioSearchRow extends Component {

    constructor(props) {
        super(props);
        this.mapItemFromProps(props);
    };

    mapItemFromProps(props) {
        const pdata = props.data;

        this.itemData = {
            id: pdata.id ? pdata.id : 0,
            link: pdata.link ? pdata.link : '#',
            title: pdata.title ? pdata.title : '',
            highlight: pdata.highlight ? pdata.highlight : '',
            app: {
                id: pdata.app && pdata.app.app_id ? pdata.app.app_id : 0,
                slug: pdata.app && pdata.app.url_label ? pdata.app.url_label : 'intra',
                name: pdata.app && pdata.app.config.name ? pdata.app.config.name : 'Intra'
            }
        };
        return this;
    };

    componentWillReceiveProps(nextProps) {
        this.mapItemFromProps(nextProps);
    };

    render() {
        const item = this.itemData;

        return (
            <a href={item.link} target="_blank" className="psr">
                <span className="psr__label">{item.app.name}</span>
                <div className="psr__desc">
                    <h2 className="psr__title">{item.title}</h2>
                    <p className="psr__highlight">{item.highlight}</p>
                </div>
            </a>
        );
    }

}

export default PodioSearchRow;

