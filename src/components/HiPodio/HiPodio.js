import React, { Component } from 'react';
import PodioAuth from '../PodioAuth';
import PodioSearch from '../PodioSearch';
import './HiPodio.scss';

class HiPodio extends Component {

    /**
     * Constructor
     * @param props , see HiPodio.propTypes for props definition
     * @returns {HiPodio}
     */
    constructor(props) {
        super(props);

        //Dynamically loadable children components
        this.childrenComp = {
            PodioAuth: this.props.PodioAuthConfig || {},
            PodioSearch: this.props.PodioSearchConfig || {}
        };

        //Auto configure extra properties
        this.childrenComp.PodioAuth.authCallback = this.returnAuthenticatedInstance.bind(this);
        this.childrenComp.PodioSearch.onListenStarts = this.onListenStarts.bind(this);
        this.childrenComp.PodioSearch.onListenEnds = this.onListenEnds.bind(this);
        this.childrenComp.PodioSearch.onHasSearch = this.onHasSearch.bind(this);
        this.childrenComp.PodioSearch.onHasNoSearch = this.onHasNoSearch.bind(this);

        //Default state definition
        this.state = {
            loggedIn: false,
            childComp: PodioAuth,
            apiInstance: null,
            logoType: 1,
            listening: false,
            hasSearch: false
        };
        return this;
    };

    /**
     * Callback that will be returned by the PodioAuth component
     * @param result , an object like this {
     *      code: 200 or 500
     *      apiInstance: the correctly authenticated api instance or false
     * }
     * @returns {HiPodio}
     */
    returnAuthenticatedInstance(result) {
        const apiInstance = result.instance;

        //console.log('returnAuthenticatedInstance', result);

        if (result.code && result.code === 200 && apiInstance !== false) {
            this.childrenComp.PodioSearch.apiInstance = apiInstance;
            //we are correctly authenticated, and may now proceed to search
            this.setState({
                loggedIn: true,
                childComp: PodioSearch,
                logoType: 2
            });
        } else {
            //We change the logotype to visually indicate an error
            this.setState({ logoType: 3 });
        }
        return this;
    }

    /**
     * Called when the Search component starts listening so we can update the hiPodio component style
     * @returns {HiPodio}
     */
    onListenStarts() {
        this.setState({listening: true});
        return this;
    }

    /**
     * Called when the Search component stops listening so we can update the hiPodio component style
     * @returns {HiPodio}
     */
    onListenEnds() {
        this.setState({listening: false});
        return this;
    }

    /**
     * Called when the Search component has an active search so we can update the hiPodio component style
     * @returns {HiPodio}
     */
    onHasSearch() {
        this.setState({hasSearch: true});
        return this;
    }

    /**
     * Called when the Search component has no active search so we can update the hiPodio component style
     * @returns {HiPodio}
     */
    onHasNoSearch() {
        this.setState({hasSearch: false});
        return this;
    }

    /**
     * Render
     * @returns {XML}
     */
    render() {
        //The current child
        const currentChild = this.state.childComp.prototype.getName();
        //And its props
        const ChildCompProps = this.childrenComp[currentChild];
        //Classes manipulations
        const compClasses = ['hiPodio', 'hiPodio__step' + currentChild]; //Auto add the active child classname.
        const logoClasses = ['hiPodio__imgwrap', 'hiPodio__imgwrap--type' + this.state.logoType];
        const podioLogo = require('../../images/podio_icon.png');

        if (this.state.listening) { compClasses.push('listening'); }
        if (this.state.hasSearch) { compClasses.push('hasSearch'); }

        return (<div className={compClasses.join(' ')}>
            <div className={logoClasses.join(' ')}>
                <img src={podioLogo} />
            </div>
            < this.state.childComp {...ChildCompProps} />
        </div>);
    };

}

/**
 * Prop types definition
 * @type {{PodioAuthConfig: *, PodioSearchConfig: *}}
 */
HiPodio.propTypes = {
    PodioAuthConfig: React.PropTypes.object.isRequired,
    PodioSearchConfig: React.PropTypes.object.isRequired
};

export default HiPodio;