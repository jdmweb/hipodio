import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {api as PodioJS, sessionStore as PodioSessionStore} from 'podio-js/lib/index.js';
import './PodioAuth.scss';

class PodioAuth extends Component {

    /**
     * Constructor
     * @param props , see PodioAuth.propTypes for props definition
     * @returns {PodioAuth}
     */
    constructor(props) {
        super(props);

        //translations
        this.translations = {
            compTitle: 'Authentification',
            title: 'HiPodio',
            subtitle: 'The Podio Listener',
            signIn: 'S\'identifier',
            login: 'Login',
            password: 'Mot de passe'
        };

        //Default state definition
        this.state = {
            loading: false,
            error: false
        };
        return this;
    };

    /**
     * Returns the component name because we can't use function.name in an uglified environment.
     * @returns {string}
     */
    getName() {
        return 'PodioAuth';
    };

    /**
     * Mouting the component
     * @returns {PodioAuth}
     */
    componentDidMount() {
        const t = this;
        const authConfig = {
            authType: this.props.authType,
            clientId: this.props.clientId
        };

        //console.log('Mounting auth');
        //If it's a password auth type, we also need to send podio the client secret
        if (this.props.authType === 'password') {
            authConfig.clientSecret = this.props.clientSecret;
        }

        //Create the api instance
        t.podio = new PodioJS(
            authConfig,
            {
                sessionStore: PodioSessionStore, //The token will be stored on local storage
                onTokenWillRefresh: t.onTokenWillRefresh.bind(this) //This will be called when the token needs refreshing
            }
        );
        //Listen to auth events
        t.registerEvents();

        //Check if we have a valid instance already
        t.checkAuth();

        return this;
    };

    /**
     * Listen to auth events
     * @returns {PodioAuth}
     */
    registerEvents() {
        const t = this;

        //Called when the auth is successful
        window.onAuthCompleted = () => {
            t.onAuthCompleted();
        };
        //Called when the auth didn't work
        window.onAuthError = () => {
            t.onAuthError();
        };

        return this;
    }

    /***
     * Click handler for the auth link,
     * which is displayed if we never had authenticated before
     * @param e Event
     */
    onStartAuthClick() {
        const t = this;

        //console.log('onStartAuthClick');
        //Check auth
        t.podio.isAuthenticated().then(() => {
            t.onAuthCompleted();
        }).catch(() => {
            t.onAuthError();
        });
        t.openPopup();

        return this;
    }

    /**
     * Called when submitting the auth form
     * @param event
     * @returns {PodioAuth}
     */
    onStartAuthSumbit(event) {
        event.preventDefault();

        //Get username and password values from refs inside the component
        const t = this;
        const username = ReactDOM.findDOMNode(this.refs.login).value;
        const password = ReactDOM.findDOMNode(this.refs.password).value;

        t.setState({ loading: true, error: false });
        //Authenticate with credentials
        t.podio.authenticateWithCredentials(username, password, (err, res) => {
            if (!err && res.access_token) {
                //Success
                t.setState({loading: false});
                t.onAuthCompleted();
            } else {
                //Error
                t.setState({ loading: false, error: true });
                t.onAuthError();
            }
        });

        return this;
    }

    /***
     * Open a popup and retrieve a new auth token,
     * since there is no support for refreshing
     */
    openPopup() {
        const t = this;
        const popup = window.open(t.podio.getAuthorizationURL(t.props.redirectUrl), 'auth_popup');

        //give user a chance to enter a password if not signed in
        popup.focus();

        return this;
    }

    /**
     * Success auth callback
     * @returns {PodioAuth}
     */
    onAuthCompleted() {
        const t = this;

        //console.log('onAuthCompleted');
        t.podio.refreshAuthFromStore();
        const Result = {
            code: 200,
            instance: t.podio
        };

        //Pass it to parent via callback
        t.props.authCallback(Result);

        return this;
    }

    /**
     * Error auth callback
     * @returns {PodioAuth}
     */
    onAuthError() {
        const t = this;

        //console.log('onAuthError');
        if (window.opener) {
            t.openPopup();
        } else {
            const Result = {
                code: 200,
                instance: false
            };

            //Pass it to parent via callback
            t.props.authCallback(Result);
        }

        return this;
    }

    /**
     * Check if we already have a valid auth instance when mouting the component
     * @returns {PodioAuth}
     */
    checkAuth() {
        const t = this;
        const caller = window.opener ? window.opener : window;

        //will fetch the tokens from the hash fragment
        //and store them in the sessionStore
        t.podio.isAuthenticated().then(() => {
            caller.onAuthCompleted();
        }).catch(() => {
            caller.onAuthError();
        });

        if (window.opener) {
            window.close();
        }

        return this;
    }

    /***
     * Will be called by the PlatformSDK when a token expires
     * @param callback Function, which will execute an original request with all its parameters when called
     */
    onTokenWillRefresh(callback) {
        const t = this;

        console.log('onTokenWillRefresh');
        //methods are registered globally for the popup to call on this main window
        window.onAuthCompleted = () => {
            //the platform SDK instance from the popup has
            //received a new auth token and saved it to the store.
            //Let's retrieve it for this instance.
            t.podio.refreshAuthFromStore();
            callback();
        };
        window.onAuthError = () => {
            t.onAuthError();
        };

        t.openPopup();

        return this;
    }

    /**
     * When unmounting the component, deregister event listeners
     * @returns {PodioAuth}
     */
    componentWillUnmount() {
        window.onAuthCompleted = null;
        window.onAuthError = null;

        return this;
    }

    /**
     * Render
     * @returns {XML}
     */
    render() {
        const compClasses = ['hiPodioAuth'];

        if (this.state.loading) { compClasses.push('loading'); }
        if (this.state.error) { compClasses.push('error'); }

        return (
            <div className={compClasses.join(' ')}>
                <h2>{this.translations.title}</h2>
                <h3>{this.translations.subtitle}</h3>
                {
                    this.props.authType === 'password' ?
                        <form onSubmit={this.onStartAuthSumbit.bind(this)}>
                            <div className="fieldWrap hasPlaceholder">
                                <label htmlFor="podio-login">{ this.translations.login }</label>
                                <input name="podio-login" type="text" className="text" ref="login" placeholder={ this.translations.login } />
                            </div>
                            <div className="fieldWrap hasPlaceholder">
                                <label htmlFor="podio-pwd">{ this.translations.password }</label>
                                <input name="podio-pwd" type="password" className="text" ref="password" placeholder={ this.translations.password } />
                            </div>
                            <button className="flatbutton blue">
                                {this.translations.signIn}
                            </button>
                        </form> :
                        <button className="flatbutton blue" onClick={this.onStartAuthClick.bind(this)}>
                            {this.translations.signIn}
                        </button>
                }
            </div>
        );
    }

}

/**
 * Prop types definition
 * @type {{authType: *, clientId: *, clientSecret: *, redirectUrl: *, authCallback: *}}
 */
PodioAuth.propTypes = {
    authType: React.PropTypes.string.isRequired,
    clientId: React.PropTypes.string.isRequired,
    clientSecret: React.PropTypes.string,
    redirectUrl: React.PropTypes.string.isRequired,
    authCallback: React.PropTypes.func.isRequired
};

export default PodioAuth;