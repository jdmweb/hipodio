import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PodioSearchRow from '../PodioSearchRow';
import './PodioSearch.scss';

class PodioSearch extends Component {

    /**
     * Constructor
     * @param props , see PodioSearch.propTypes for props definition
     * @returns {PodioSearch}
     */
    constructor(props) {
        super(props);
        this.apiInstance = this.props.apiInstance;

        //Translations
        this.translations = {
            title: 'Search',
            listening: 'HiPodio is listening',
            hint: 'You can type in the search input or click the speaker and speak',
            newSearch: 'New search',
            resultFor: 'result for',
            resultsFor: 'results for'
        };

        //Default state definition
        this.state = {
            inputVal: null,
            inputPlaceholder: this.translations.title,
            currentSearch: null,
            searching: false,
            searchResults: []
        };
        this.initRecognition();
        return this;
    }

    /**
     * Returns the component name because we can't use function.name in an uglified environment.
     * @returns {string}
     */
    getName() {
        return 'PodioSearch';
    };

    /**
     * Init the speech recognition instance
     * @returns {PodioSearch}
     */
    initRecognition() {
        const t = this;

        if ('webkitSpeechRecognition' in window) {
            t.recognition = new webkitSpeechRecognition();
            t.recognition.lang = 'fr-FR';
            t.recognition.onresult = function (event) {
                const q = event.results[0][0]['transcript'];

                t.queryApi(q);
                t.onListenEnds();
            };
            t.recognition.onend = function () {
                t.onListenEnds();
            };
            t.recognition.onerror = function (err) {
                t.onListenEnds();
                if (err.error === 'not-allowed') {
                    //Pbm micro
                }
            };
        }
        return this;
    }

    /**
     * Launch the search when submitting the search form
     * @param e , event
     * @returns {PodioSearch}
     */
    launchSearch(e) {
        console.log('launchSearch');

        e.preventDefault();

        //find search val
        const q = ReactDOM.findDOMNode(this.refs.q).value;

        //console.log(q);

        if (q && q.length > 0 && q !== this.translations.newSearch && q !== this.translations.title) {
            this.queryApi(q);
        }
        return this;
    }

    /**
     * Start listening via the speech recognition
     * @returns {PodioSearch}
     */
    onListenStarts() {
        this.recognition.stop();
        this.props.onListenStarts && this.props.onListenStarts();
        this.setState({inputVal: '', inputPlaceholder: this.translations.listening});
        this.recognition.start();
        return this;
    }

    /**
     * Callback when the voice recognition ends
     * @returns {PodioSearch}
     */
    onListenEnds() {
        this.setState({inputVal: this.translations.title});
        this.props.onListenEnds && this.props.onListenEnds();
        return this;
    }

    /**
     * Callback when we have a search in the state
     * @returns {PodioSearch}
     */
    onHasSearch() {
        this.props.onHasSearch && this.props.onHasSearch();
        return this;
    }

    /**
     * Callback when we have no search in the state
     * @returns {PodioSearch}
     */
    onHasNoSearch() {
        this.props.onHasNoSearch && this.props.onHasNoSearch();
        return this;
    }

    /**
     * Query the podio api with the search
     * @param q , the search query
     * @returns {PodioSearch}
     */
    queryApi(q) {
        const t = this;

        t.setState({
            searching: true,
            currentSearch: q
        });

        //Call the API
        t.apiInstance.request('GET', `/search/v2?counts=false&highlights=false&limit=10&offset=0&query=${q}`)
            .then((responseData) => {
                t.onHasSearch();
                t.setState({
                    inputVal: null,
                    searching: false,
                    searchResults: responseData.results
                });
            });
        return this;
    }

    /**
     * Render
     * @returns {XML}
     */
    render() {
        const compClasses = ['podioSearch'];
        const resultRows = (this.state.currentSearch !== null && this.state.searchResults.length > 0) ? this.state.searchResults.map((res, index) => {
            return (<PodioSearchRow data={res} key={index}/>);
        }) : '';

        if (this.state.searching) { compClasses.push('searching'); }

        return (
            <div className={compClasses.join(' ')}>
                <div className="podioSearch__search-zone">
                    <form onSubmit={this.launchSearch.bind(this)}>
                        <input className="text podioSearch__text-trigger" type="text" ref="q" value={this.state.inputVal} placeholder={this.state.inputPlaceholder} />
                    </form>
                    {('webkitSpeechRecognition' in window) ? <button className="podioSearch__voice-trigger" onClick={this.onListenStarts.bind(this)}>Speak</button> : ''}
                </div>
                <div className="podioSearch__results">
                    <div className="podioSearch__results-summary">
                        <span className="podioSearch__results-summary-count">{ this.state.searchResults.length }</span>
                        <span className="podioSearch__results-summary-for">{ (this.state.searchResults.length > 1) ? this.translations.resultsFor : this.translations.resultFor }</span>
                        <span className="podioSearch__results-summary-term">{ this.state.currentSearch }</span>
                    </div>
                    {resultRows}
                </div>
                <span className="help">{this.translations.hint}</span>
            </div>
        );
    }

}

/**
 * Prop types definition
 * @type {{apiInstance: *, onListenStarts: *, onListenEnds: *, onHasSearch: *, onHasNoSearch: *}}
 */
PodioSearch.propTypes = {
    apiInstance: React.PropTypes.object.isRequired,
    onListenStarts: React.PropTypes.func,
    onListenEnds: React.PropTypes.func,
    onHasSearch: React.PropTypes.func,
    onHasNoSearch: React.PropTypes.func
};

export default PodioSearch;
