import React from 'react';
import ReactDOM from 'react-dom';
import HiPodio from './components/HiPodio';
import HiPodioConfig from './components/HiPodio/package.json';

window.onload = () => {
    ReactDOM.render(
        <HiPodio PodioAuthConfig={HiPodioConfig.config.auth} PodioSearchConfig={HiPodioConfig.config.search} />,
        document.querySelector('#container')
    );
};
